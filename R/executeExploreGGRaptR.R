#' Perform the required operations for displaying interactive plot generator.
#' 
#' Time-stamp: <2017-03-06 11:04:49 Graham Williams>
#' 
executeExploreGGRaptR <- function(dataset.name)
{
  # Check prerequisite packages.

  if (!packageIsAvailable("ggraptR", 
                          Rtxt("interactively generate ggplot2 graphics")))
    return(FALSE)
  
  startLog(Rtxt("Display interactive plot builder."))

  cmd <- sprintf('ggraptR::ggraptR(%s, port=5002)', dataset.name)
  appendLog("Initiate the ggraptR application in a browser", cmd)
  system(sprintf('R -q -e "%s"', cmd), wait=FALSE, intern=FALSE)  # debug: wait=intern=T + print
  
  return()
}
